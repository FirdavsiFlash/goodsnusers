const express = require("express")
const Goods = require("../models/goods")
const router = express.Router()

// Так пишем сейчас
router.get("/", async (req, res) => {
    try {
        let body = await Goods.find().populate("authorId", "categoryId")
        res.json({
            title: "goods",
            description: "Here is all goods",
            isDone: true,
            body
        })
    } catch (error) {
        console.log(error)

        res.json({
            ok: false,
            messege: "Seems there no goods!",
            error
        })
    }
})

router.get("/:id", async (req, res) => {

    try {

        Goods.findByIdAndUpdate(req.params.id, {
            $inc: {
                views: 1
            }
        }, {
            new: true
        }, async (err, data) => {
            if (err) {
                console.log(err);
            } else {


                res.json({
                    ok: true,
                    message: "One element got",
                    element: data
                })
            }
        })
    } catch (error) {
        console.log(error);
    }
})

router.post("/", (req, res) => {
    try {

        Goods.create(req.body, async (error, data) => {
            if (error) {
                res.json({
                    ok: false,
                    messege: "Seems there no goods!",
                    error
                })
            } else {
                res.json({
                    ok: true,
                    message: 'created!',
                    element: req.body,
                })
            }
        })
    } catch (error) {
        res.json({
            ok: false,
            messege: "Seems there no goods!",
            error
        })
    }
})

router.patch("/:id", async (req, res) => {
    try {
        Goods.findOneAndUpdate(req.params.id, req.body, (error, data) => {
            if (error) {
                console.log(error);
            } else {
                res.json({
                    ok: true,
                    message: "Updated",
                    element: data
                })
            }
        })
    } catch (error) {
        res.json({
            ok: false,
            message: "Couldn't change good",
            error
        })
    }
})

router.delete("/:id", async (req, res) => {
    Goods.findByIdAndDelete(req.params.id, async (error, data) => {
        if (error) {
            res.json({
                ok: false,
                messege: "Deleted shit!",
                el: data,
                error
            })
        } else {
            res.json({
                ok: true,
                message: 'Deleted',
                element: data,
            })
        }
    })
})

module.exports = router