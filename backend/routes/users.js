const express = require("express")
const Users = require("../models/users")
const router = express.Router()

// Так пишем сейчас
router.get("/", async (req, res) => {

    try {
        let body = await Users.find()

        res.json({
            title: "goods",
            description: "User/s found",
            isDone: true,
            body
        })
    } catch (error) {
        console.log(error)

        res.json({
            ok: false,
            messege: "Cant find user/s!",
            error
        })
    }
})

router.get("/:id", async (req, res) => {
    try {
        let body = await Users.find().populate("products")
        Users.findByIdAndUpdate(req.params.id, {
            $inc: {
                productsCount: 1
            }
        }, {
            new: true
        }, async (err, data) => {
            if (err) {
                console.log(err);
            } else {
                res.json({
                    ok: true,
                    message: "One element got",
                    element: data,
                    body
                })
            }
        })
    } catch (error) {
        console.log(error);
    }
})

router.post("/", (req, res) => {
    try {

        Users.create(req.body, async (error, data) => {
            if (error) {
                res.json({
                    ok: false,
                    messege: "Seems there no goods!",
                    error
                })
            } else {
                res.json({
                    ok: true,
                    message: 'created!',
                    element: data,
                })
            }
        })
    } catch (error) {
        res.json({
            ok: false,
            messege: "Seems there no goods!",
            error
        })
    }
})

router.patch("/:id", async (req, res) => {
    try {
        Users.findOneAndUpdate(req.params.id, req.body, (error, data) => {
            if (error) {
                console.log(error);
            } else {
                res.json({
                    ok: true,
                    message: "Updated",
                    element: data
                })
            }
        })
    } catch (error) {
        res.json({
            ok: false,
            message: "Couldn't change user",
            error
        })
    }
})

router.delete("/:id", async (req, res) => {
    Users.findByIdAndDelete(req.params.id, async (error, data) => {
        if (error) {
            res.json({
                ok: false,
                messege: "Deleted shit!",
                el: data,
                error
            })
        } else {
            res.json({
                ok: true,
                message: 'Deleted',
                element: data,
            })
        }
    })
})
module.exports = router