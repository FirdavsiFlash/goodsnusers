const mongoose = require('mongoose')

// Называние схем всегда во множественном числе
// Ключи вашей схемы крайне опасно менять/удалять
const Goods = mongoose.Schema({
    authorId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Users",
        required: true
    },
    name: {
        type: String,
        required: true
    },
    views: {
        type: Number,
        default: 0
    },
    description: {
        type: String,
    },
    price: {
        type: Number,
        required: true
    },
    publisher: {
        type: String,
        required: true
    },
    url: {
        type: String
    }
})

module.exports = mongoose.model('Goods', Goods)