const mongoose = require('mongoose')

var Users = mongoose.Schema({
    name: {
        type: String,
        required: false,
        default: "Adam Adams"
    },
    password: {
        type: String,
        required: true
    },
    url: {
        type: String,
        default: "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIAIUAhQMBIgACEQEDEQH/xAAbAAEAAQUBAAAAAAAAAAAAAAAABQEDBAYHAv/EADsQAAIBAgIGBwQHCQAAAAAAAAABAgMEBRESITFBUdEGFiJUYZGSFHGBoRMjNEKxwfAVMkRSU2JyguH/xAAUAQEAAAAAAAAAAAAAAAAAAAAA/8QAFBEBAAAAAAAAAAAAAAAAAAAAAP/aAAwDAQACEQMRAD8AnwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD3SpVK0tGlTnOXCEW/wJvo/gPt0Vc3eat8+zDY58kbhQoUremqdCnGnBbIxWSA51PDr6EdKVncJLa3SkYz1NrgdReZg4jhNpiEGq1NKplqqR1SXMDngMrErCth11KhW174yWyS4mKAAAAAAAAAAAAAADIw+29svqFv/AFJZP3b/AJGOSvRfJY3Q0uEsvfosDe6cI04RhBKMYpJJbkVzDCAqAUesCG6UWiucMnUS7dv24vw3/rwNHOkYlorDrrS2fQzz8mc3WwAAAAAAAAAAAAAAF6yuHa3dG4jrdOSeXFbyyAOnW9WFelCtSkpU5pSi1vRcNFwLG54c/oaqdS2bzyW2D4rkbhaX9rewUratGa4Z618AMnMqUzSWbIrE8fs7KDUJqvW2KEHms/F7gLPSu+jb4c7dP6yv2cs9kd7/AC+JpJfvbute3Eq9xLSnLyiuCLAAAAAAAAAAAAAAAB7pU51qkadKEpzk8lGKzbJ/D+i1WplO+qqkv5KeTl57F8wNdGaz26zoFtgWG26WjbRm196p2n8zPhSp01lThGC4RikBzOU6rjlOU2v7m8i2svA6llmWa1nbV1lWt6U/8oJgc0Bu950ZsKybo6dCXGDzXkzXMSwK8sE56KrUV9+nu963ARYAAAAAAAAAAF+ytKt7cwoUFnOXkvFlg3XopYK3sPaJL62vrz4R3L8wM3CsLoYbS0aS0qjXbqvbLkvAkAgAPO1jaegABRrMBtG4qUYGs9IcAjKMrqxglJa50l95cV4+Bqh1HaaL0msVZYi3TjlSrLTilue9friBEAAAAAPMZxlOUU9cXrWWw9GNQ+23P+v4GSAexnTLOCp2lCEdkacUvI5k9jN8oY/hcaNOMrpZqKT7EuHuAlwRTx/C3/Fr0S5FesGFd7XolyAlARfWDCu9r0S5FP2/hef2teiXICUWsqRfWDCu9r0S5DrDhXe16JcgJQEV1gwvva9EuRXrBhXe16JcgJQ1nptFez2s96qSXy/4SXWDC912vRLkQnSnErO+tqELWsqko1HJrRa1ZeKA1wAAAABj0Uld3GSafZbz3mQY9HL2y5yUs8oZ5+4yAAAAAAAAAAAAAAAAAAAAAADxClGFWpVX708s/gewAAAAAAAAAAAAAAAAAAW8AAAAP//Z"
    },
    products: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Goods",
    }],
    productsCount: {
        type: Number,
        default: 0
    }
})


module.exports = mongoose.model('Users', Users)